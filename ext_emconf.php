<?php

########################################################################
# Extension Manager/Repository config file for ext "pxa_solr".
#
# Auto generated 24-05-2012 16:12
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
    'title' => 'Pixelant solr search',
    'description' => 'Search with solr - fastest and feature rich searching server.',
    'category' => 'plugin',
    'author' => 'Jozef Spisiak, Robert Lindh',
    'author_email' => 'jozef@pixelant.se, robert@pixelant.se',
    'author_company' => 'Pixelant, Pixelant',
    'shy' => '',
    'priority' => '',
    'module' => '',
    'state' => 'beta',
    'internal' => '',
    'uploadfolder' => 0,
    'createDirs' => '',
    'modify_tables' => '',
    'clearCacheOnLoad' => 0,
    'lockType' => '',
    'version' => '3.0.0.2',
    'constraints' => array(
        'depends' => array(
            'cms' => '6.2.0-6.2.99',
            'extbase' => '6.2.0-6.2.99',
            'fluid' => '',
            'solr' => '3.0.0-0.0.0',
        ),
        'conflicts' => array(
        ),
        'suggests' => array(
        ),
    ),
    '_md5_values_when_last_written' => 'a:35:{s:21:"ExtensionBuilder.json";s:4:"0228";s:12:"ext_icon.gif";s:4:"e146";s:17:"ext_localconf.php";s:4:"1b81";s:14:"ext_tables.php";s:4:"7d3e";s:14:"ext_tables.sql";s:4:"0c48";s:28:"ext_typoscript_constants.txt";s:4:"7d5c";s:24:"ext_typoscript_setup.txt";s:4:"865a";s:39:"Classes/Controller/SearchController.php";s:4:"bafb";s:43:"Classes/ViewHelpers/HighlightViewHelper.php";s:4:"f73e";s:44:"Configuration/ExtensionBuilder/settings.yaml";s:4:"a4b3";s:40:"Configuration/FlexForms/flexform_pi1.xml";s:4:"9c62";s:38:"Configuration/TypoScript/constants.txt";s:4:"6a5d";s:34:"Configuration/TypoScript/setup.txt";s:4:"3016";s:40:"Resources/Private/Language/locallang.xml";s:4:"c1b5";s:43:"Resources/Private/Language/locallang_db.xml";s:4:"6d40";s:36:"Resources/Private/Partials/case.html";s:4:"cba4";s:42:"Resources/Private/Partials/discussion.html";s:4:"8f5f";s:40:"Resources/Private/Partials/fe_users.html";s:4:"ab68";s:38:"Resources/Private/Partials/member.html";s:4:"e1cf";s:36:"Resources/Private/Partials/page.html";s:4:"eb0c";s:39:"Resources/Private/Partials/tt_news.html";s:4:"ab33";s:44:"Resources/Private/Partials/tx_solr_file.html";s:4:"a9f0";s:46:"Resources/Private/Templates/Search/result.html";s:4:"0852";s:49:"Resources/Private/Templates/Search/searchbox.html";s:4:"fb2d";s:32:"Resources/Public/Css/pxasolr.css";s:4:"1b24";s:35:"Resources/Public/Icons/relation.gif";s:4:"e615";s:33:"Resources/Public/Images/noimg.gif";s:4:"9821";s:37:"Resources/Public/Images/nonewsimg.jpg";s:4:"6599";s:37:"Resources/Public/Images/preloader.gif";s:4:"3e3d";s:48:"Resources/Public/Scripts/jquery-ui.custom.min.js";s:4:"3ceb";s:46:"Resources/Public/Scripts/jquery.history.min.js";s:4:"b9b7";s:38:"Resources/Public/Scripts/jquery.min.js";s:4:"9c25";s:38:"Resources/Public/Scripts/moc_search.js";s:4:"ad8a";s:35:"Resources/Public/Scripts/suggest.js";s:4:"a2bc";s:14:"doc/manual.sxw";s:4:"8d2d";}',
);
