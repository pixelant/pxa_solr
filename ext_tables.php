<?php
if (!defined('TYPO3_MODE')) {
    die('Access denied.');
}

Tx_Extbase_Utility_Extension::registerPlugin(
    $_EXTKEY,
    'Searchbox',
    'Search box with results'
);

Tx_Extbase_Utility_Extension::registerPlugin(
    $_EXTKEY,
    'Searchresult',
    'Typoscript use only. Do not add this plugin on page. Returns JSON array.'
);

t3lib_extMgm::addStaticFile($_EXTKEY, 'Configuration/TypoScript', 'Pixelant solr search');

$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['pxasolr_searchbox'] = 'pi_flexform';
t3lib_extMgm::addPiFlexFormValue('pxasolr_searchbox', 'FILE:EXT:' . $_EXTKEY . '/Configuration/FlexForms/flexform_pi1.xml');
