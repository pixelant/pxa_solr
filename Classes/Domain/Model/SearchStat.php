<?php
namespace Pixelant\PxaSolr\Domain\Model;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Mats Svensson <mats@pixelant.se>, Pixelant
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * SearchStat
 *
 * @package pxa_solr
 * @license http://www.gnu.org/licenses/gpl.html GNU General Public License, version 3 or later
 *
 */

class SearchStat extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * @var string
     */
    protected $searchword;

    /**
     * @var string
     */
    protected $category;

    /**
     * @var integer
     */
    protected $mostRecentAmount;

    /**
     * @var integer
     */
    protected $performedSearches;
    
    /**
     * Get searchword
     *
     * @return string
     */
    public function getSearchword()
    {
        return $this->searchword;
    }

    /**
     * Set searchword
     *
     * @param string $searchword
     * @return void
     */
    public function setSearchword($searchword)
    {
        $this->searchword = $searchword;
    }

    /**
     * Get category
     *
     * @return string
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set category
     *
     * @param string $category
     * @return void
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * Get most recent amount
     *
     * @return integer
     */
    public function getMostRecentAmount()
    {
        return $this->mostRecentAmount;
    }

    /**
     * Set most recent amount
     *
     * @param integer $mostRecentAmount
     * @return void
     */
    public function setMostRecentAmount($mostRecentAmount)
    {
        $this->mostRecentAmount = $mostRecentAmount;
    }

    /**
     * Get performed searches
     *
     * @return integer
     */
    public function getPerformedSearches()
    {
        return $this->performedSearches;
    }

    /**
     * Set performed searches
     *
     * @param integer $performedSearches
     * @return void
     */
    public function setPerformedSearches($performedSearches)
    {
        $this->performedSearches = $performedSearches;
    }
}
