<?php

class Tx_PxaSolr_Controller_SearchController extends Tx_Extbase_MVC_Controller_ActionController
{

    /**
    * @var tx_solr_Search
    */
    protected $search;

    /**
    * @var tx_solr_Query
    */
    protected $query;

    /**
     * searchStatRepository
     *
     * @var Pixelant\PxaSolr\Domain\Repository\SearchStatRepository
     * @inject
     */
    protected $searchStatRepository;

    /**
     * persistence manager
     *
     * @var \TYPO3\CMS\Extbase\Persistence\PersistenceManagerInterface
     * @inject
     */
    protected $persistenceManager;


    public function initializeAction()
    {

        // make the current id available to fluid-template.
        $this->settings['currentPid'] = $GLOBALS['TSFE']->id;

        // Check that resultPid is set - else set it to the current page
        if (empty($this->settings['resultPid'])) {
            $this->settings['resultPid'] = $GLOBALS['TSFE']->id;
        }
    }

    /**
    * @return string
    */
    public function searchboxAction()
    {
        $this->view->assign('settings', $this->settings);
        $this->view->assign('formUrl', $this->getFormActionUrl());
        $this->view->assign('suggestUrl', $this->getSuggestUrl());
        $this->view->assign('formJsonUrl', $this->getFormJsonActionUrl());

        $this->view->assign('tags', $this->getSearchTags());
        $this->view->assign('loggedIn', $GLOBALS['TSFE']->loginUser?'1':'0');

        $tempArray = t3lib_div::_POST('tx_pxasolr_searchbox');
        $this->view->assign('postsearchValue', $tempArray['sword']);
    }

    /**
    * @return string
    */
    public function quicksearchAction()
    {
        $this->view->assign('settings', $this->settings);
        $this->view->assign('formUrl', $this->getFormActionUrl());
        $this->view->assign('suggestUrl', $this->getSuggestUrl());
        $this->view->assign('formJsonUrl', $this->getFormJsonActionUrl());

        $this->view->assign('tags', $this->getSearchTags());
        $this->view->assign('loggedIn', $GLOBALS['TSFE']->loginUser?'1':'0');

        $tempArray = t3lib_div::_POST('tx_pxasolr_searchbox');
        $this->view->assign('postsearchValue', $tempArray['sword']);
    }

    /**
    * @return string
    */
    private function getAdditionalFilters()
    {
        $additionalFilters = array();
        if (is_array($this->settings['additionalFilters'])) {
            foreach ($this->settings['additionalFilters'] as $searchTag) {
                foreach ($searchTag as $filter => $filterValue) {
                    $additionalFilters[] = $filter . ':' . $filterValue;
                }
            }
        }
        return $additionalFilters;
    }

    /**
    * @param string $sword
    * @param string $searchTag
    * @param integer $page
    * @return string
    */
    public function resultAction($sword = '', $searchTag = '', $page = 1)
    {

            //Check that a valid searchTag is given
        if (!in_array($searchTag, $this->getSearchTags(1))) {
            return json_encode(array());
        }

            // Fetch configuration
        if ($GLOBALS['TYPO3_CONF_VARS']['SYS']['compat_version'] <= 4.7) {
            $configuration = Tx_Extbase_Dispatcher::getExtbaseFrameworkConfiguration();
        } else {
            $configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        }

            // make searchword lowercase
        $sword = strtolower(urldecode($sword));

        // add * if it's missing and if the length is less than 8 chars (to prevent not getting any search result)
        if ((substr($sword, -1) != "*") && (strlen($sword) < 5)) {
            $temp_sword = $sword;
            $sword .= '*'.' OR '.$sword;
        }
        if (strlen($sword) == 0) {
            return json_encode(array());
        }

        $this->search = t3lib_div::makeInstance('tx_solr_Search');

        $this->query = t3lib_div::makeInstance('tx_solr_Query', $sword);

        $sword = $temp_sword ? $temp_sword : $sword;
        if (substr($sword, -1) == "*") {
            $sword = substr($sword, 0, strlen($sword)-1);
        }

        if ($searchTag != 'otherSites') {
            $this->query->addFilter('type:' . $searchTag);
        } else {
            $this->query->addFilter('type:pages');
        }

        // Additional filter
        if (!empty($this->settings['additionalFilters'])) {
            if (isset($this->settings['additionalFilters'][$searchTag])) {
                foreach ($this->settings['additionalFilters'][$searchTag] as $filter => $filterValue) {
                    $this->query->addFilter($filter.':'.$filterValue);
                }
            }
        }
        /////////////////

        $returnFields = $this->settings['specialTags'][$searchTag]['returnFields'];
        if (is_array($returnFields) && (!empty($returnFields))) {
            $returnFields[] = 'score';
            $this->query->setFieldList();
        }

        //$this->query->addFilter('language:' . intval (t3lib_div::_GP('L')));

        if ($searchTag != 'otherSites') {
            // Selects rootline-uid and picks the first/top domain from there and filters the result according to that.
            $rootPageId = $GLOBALS['TSFE']->rootLine[0]['uid'];
            $result = $GLOBALS['TYPO3_DB']->exec_selectQuery('domainName', 'sys_domain', 'pid = '.$rootPageId.' AND hidden = 0', '', 'sorting', '0,1');
            $record = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);

            // get baseURL and strip http*:// and remove trail slash.
            if (!empty($record['domainName'])) {
                $cleanUrl = rtrim(preg_replace("/(http:\/\/)/i", "", $record['domainName']), "/");
                $this->query->addFilter('site:'.$cleanUrl);
            }
        } else {
            $sitelist = implode(' OR ', $this->settings['specialTags']['otherSites']['domains']);
            if ($sitelist != '') {
                $this->query->addFilter('site:('.$sitelist.')');
            }
        }

        // Init settings
        $resultsPerPage = $this->settings['paging']['resultsPerPage'];
        $noPagesToShow = $this->settings['paging']['numberOfPagesToShow'];

        // Calculate offset from page no.
        $offset = ($page > 0) ? $resultsPerPage * ($page - 1) : 0;

        // Enable  or disable spellchecker
        if (intval($this->settings['spellChecker'])) {
            $this->query->setSpellchecking(true);
        }

        $this->search->search($this->query, $offset, $resultsPerPage);

        $solrResults = $this->search->getResultDocuments();

        $this->addAdditionalFields($solrResults, $searchTag);

        $searchResults = array();

        foreach ($solrResults as $key => $document) {
            $availableFields = $document->getFieldNames();
            foreach ($availableFields as $fieldName) {
                $searchResults[$key][$fieldName] = $document->{$fieldName};
            }
            $searchResults[$key]['cHashParams'] = array('cHash' => $searchResults[$key]['contentHash']);
        }

        $resultCount = $this->search->getNumberOfResults();

            // Check if storagePid is assigned, otherwise no logging
        if (intval($configuration['persistence']['storagePid']) > 0) {

                // Use repository to update search statistics
            $this->updateSearchStat($sword, $searchTag, $resultCount);
        }

        $jsonResults = array();

        if ($searchResults) {

                // Determine if there is a partial for current searchtag
            $partial = $this->determinePartial($searchTag, $configuration);

                // Assign template variables
            $this->view->assign('partial', $partial);
            $this->view->assign('searchTag', $searchTag);
            $this->view->assign('searchWord', $sword);

            foreach ($searchResults as $searchResult) {
                $this->view->assign('searchResult', $searchResult);

                $result = trim($this->view->render());
                if (empty($result)) {
                    $resultCount--;
                } else {
                    array_push($jsonResults, $result);
                }
            }
        }

        // Result array
        $result = array_merge(
            $this->generatePagination($page, $resultCount, $resultsPerPage, $noPagesToShow),
            array(
                'totalCount' => $resultCount,
                'results' => $jsonResults
            )
        );

        // Add spellchecker result if it is
        if (intval($this->settings['spellChecker'])) {
            $result['spellChecker'] = $this->search->getSpellcheckingSuggestions();
            if ($result['spellChecker']) {
                $result['spellChecker'] = current(current($result['spellChecker'])->suggestion);
            }
        }

        return json_encode($result);
    }

    /*
    * @param integer $page
    * @param integer $totalCount
    * @param integer $resultsPerPage
    * @param integer $noPagesToShow
    */
    private function generatePagination($page, $totalCount, $resultsPerPage, $noPagesToShow)
    {
        // Calculate numberOfPages based on the result count and resultsPerPage
        $numberOfPages = ceil($totalCount / $resultsPerPage);

        // Generate "link" to all pages
        $pages = range(1, $numberOfPages);

        // Find start page
        $pageOffset = $page - floor(($noPagesToShow - 1) / 2);

        // Correct page if negative
        $pageOffset = ($pageOffset < 1) ? 1 : $pageOffset;

        // Correct if page is to close to end
        $lastPage = $pageOffset + $noPagesToShow - 1;
        $pageOffset = ($lastPage > $numberOfPages) ? $numberOfPages-$noPagesToShow+1 : $pageOffset;

        // Correct page (again) if now negative
        $pageOffset = ($pageOffset < 1) ? 1 : $pageOffset;

        // Pagination
        $pagination = array_slice($pages, $pageOffset - 1, $noPagesToShow);

        // Add dots if necessary
        if (reset($pagination) > 1) {
            array_shift($pagination);
            array_shift($pagination);
            array_unshift($pagination, '&#8230;');
            array_unshift($pagination, 1);
        }
        if (end($pagination) < $numberOfPages) {
            array_pop($pagination);
            array_pop($pagination);
            array_push($pagination, '&#8230;');
            array_push($pagination, $numberOfPages);
        }

        // Previous page
        $prevPage = ($page > 1) ? $page - 1 : null;

        // Next page
        $nextPage = ($page < $numberOfPages) ? $page + 1 : null;

        return array(
        'prevPage' => $prevPage,
        'nextPage' => $nextPage,
        'resultsFrom' => ($resultsPerPage * ($page - 1)) + 1,
        'resultsTo' => (($resultsPerPage * $page) > $totalCount) ? $totalCount : ($resultsPerPage * $page),
        'currentPage' => (int)$page,
        'totalPages' => $numberOfPages,
        'pagination' => $pagination
        );
    }

    /**
    * @return array
    */
    private function getSearchTags($includeHidden = 0)
    {
        $searchTags = array();

        if (!empty($this->settings['sorting'])) {
            $searchTags = array_merge($searchTags, explode(',', trim($this->settings['sorting'])));
        }

        $searchTags = array_merge($searchTags, array_keys($this->settings['indexConfigurations']));
        $searchTags = array_merge($searchTags, array_keys($this->settings['specialPageIds']));
        $searchTags = array_merge($searchTags, array_keys($this->settings['specialTags']));

        foreach ($searchTags as $key => $tag) {
            $searchTags[$key] = trim($tag);
        }

        $searchTags = array_unique($searchTags);

        // removes searchTags if no translation (_LOCAL_LANG) for each value exists.

        if ($includeHidden == 0) {
            foreach ($searchTags as $key => $tag) {
                if (Tx_Extbase_Utility_Localization::translate($tag, 'pxaSolr') == false) {
                    unset($searchTags[$key]);
                }
            }
        }

        return $searchTags;
    }

    private function addAdditionalFields(&$searchResults, $searchTag)
    {
        foreach ($searchResults as &$result) {
            $record = $GLOBALS['TSFE']->sys_page->getPage($result->uid);
            $this->addImages($searchTag, $record, $result);
            $this->addTitleTag($searchTag, $record, $result);
        }
    }
    private function addImages($searchTag, &$record, &$result)
    {
        $version = class_exists('t3lib_utility_VersionNumber') ? t3lib_utility_VersionNumber::convertVersionNumberToInteger(TYPO3_version) : t3lib_div::int_from_ver(TYPO3_version);

        if (intval($this->settings['enableImages']) == 1) {
            if ($searchTag == 'pages') {
                if (empty($result->image)) {
                    // If typo3 6.0 or higher
                    if ($version >= 6000000) {
                        $this->addPageImageMedia($result, $record);
                    // If  typo3 version less then 6.0
                    } else {
                        if (!intval($this->settings['contentImagesPriority'])) {
                            if (!$this->addPageImageMedia($result, $record)) {
                                $this->addPageImageContent($result, $record);
                            }
                        } else {
                            if (!$this->addPageImageContent($result, $record)) {
                                $this->addPageImageMedia($result, $record);
                            }
                        }
                    }
                }
            } elseif ($searchTag == 'tt_news') {
                if (empty($result->image)) {
                    $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('image', 'tt_news', 'uid = '.intval($result->uid), '', '', 1);
                    if (count($rows)) {
                        $row = current($rows);

                        $image = $GLOBALS['TSFE']->cObj->listNum($row['image'], 0);

                            // Load columns for tt_news if not already loaded
                            if (!isset($GLOBALS['TCA']['tt_news']['columns'])) {
                                t3lib_div::loadTCA('tt_news');
                            }
                        if ($image) {
                            $result->image = $GLOBALS['TCA']['tt_news']['columns']['image']['config']['uploadfolder'] . '/' . $image;
                        }
                    }
                }
            } elseif ($searchTag == 'tx_pxaaimpoint_products') {
                if (empty($result->image)) {
                    $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('image', 'tx_pxaaimpoint_products', 'uid = '.intval($result->uid), '', '', 1);
                    if (count($rows)) {
                        $row = current($rows);

                        $image = $GLOBALS['TSFE']->cObj->listNum($row['image'], 0);

                            // Load columns for tt_news if not already loaded
                            if (!isset($GLOBALS['TCA']['tx_pxaaimpoint_products']['columns'])) {
                                t3lib_div::loadTCA('tx_pxaaimpoint_products');
                            }
                        if ($image) {
                            $result->image = $GLOBALS['TCA']['tx_pxaaimpoint_products']['columns']['image']['config']['uploadfolder'] . '/' . $image;
                        }
                    }
                }
            }
        }
    }

    private function addPageImageMedia(&$result, &$record)
    {

        // Get typo3 version
        $version = class_exists('t3lib_utility_VersionNumber') ? t3lib_utility_VersionNumber::convertVersionNumberToInteger(TYPO3_version) : t3lib_div::int_from_ver(TYPO3_version);

        if (intval($this->settings['enableMediaImages']) == 1) {

            // If typo3 version is 6.0 or highter
            if ($version >= 6000000) {

                // And record has related media
                            if (is_numeric($record['media']) && $record['media'] > 0) {

                    // Get files related to the record
                                    $fileRepository = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance('TYPO3\CMS\Core\Resource\FileRepository');
                                $mediaFiles = $fileRepository->findByRelation('pages', 'media', $record['uid']);

                    // Get first file properties
                                        $fileProperties = $mediaFiles[0]->getOriginalFile()->getProperties();

                    // Get file path
                                        switch ($fileProperties['storage']) {
                                            case 0:
                                                    $src = 'uploads' . $fileProperties['identifier'];
                                                        break;
                                                default:
                                                    $src = 'fileadmin' . $fileProperties['identifier'];
                                                        break;
                                           }

                                $result->image = $src;

                                return true;
                            }
            } else {
                if (!empty($record['media'])) {
                    // Load columns for pages if not already loaded
                    if (!isset($GLOBALS['TCA']['pages']['columns'])) {
                        t3lib_div::loadTCA('pages');
                    }
                    // Set uploadfolder to the media's config's uploadfolder
                    $uploadfolder = isset($GLOBALS['TCA']['pages']['columns']['media']['config']['uploadfolder']) ? $GLOBALS['TCA']['pages']['columns']['media']['config']['uploadfolder'] : 'uploads/media';
                    // Set image to relative path and select the first image from the media field
                    $image = $GLOBALS['TSFE']->cObj->listNum($record['media'], 0);
                    if ($image) {
                        $result->image = $uploadfolder . '/' . $image;
                        return true;
                    }
                }
            }
        }

        return false;
    }

    private function addPageImageContent(&$result, &$record)
    {
        if (intval($this->settings['enableContentImages']) == 1) {
            if (!empty($record['tx_templavoila_flex'])) {
                $flex = new SimpleXMLElement($record['tx_templavoila_flex']);

                $fields = $flex->xpath("//field");

                $valuesArr = array();
                foreach ($fields as $field) {
                    $valuesArr[] = (string)$field->value;
                }

                $valuesArr = array_filter($GLOBALS['TYPO3_DB']->cleanIntArray($valuesArr));
                $values = implode(',', $valuesArr);

                $pid = intval($record['uid']);

                if ($values) {
                    $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows('uid, image', 'tt_content', 'uid in (' . $values . ') and cType in("textpic", "image") and pid = '.$pid, '', 'find_in_set(uid, "' . $values . '")', 1);

                    if (count($rows) > 0) {
                        $row = current($rows);

                        // Load DAM config from TCA
                        $damConfig = $GLOBALS['TCA']['tt_content']['columns']['tx_damttcontent_files']['config'];
                        if ($damConfig) {
                            // Get dam files for the tt_content element
                            $filesArray = tx_dam_db::getReferencedFiles('tt_content', $row['uid'], $damConfig['MM_match_fields'], $damConfig['MM'], 'tx_dam.*');
                            // Set the image to the first tt_content element file
                            $image = array_pop($filesArray['files']);
                            if ($image) {
                                $result->image = $image;
                                return true;
                            }
                        } else {
                            // Load columns for tt_content if not already loaded
                            if (!isset($GLOBALS['TCA']['tt_content']['columns'])) {
                                t3lib_div::loadTCA('tt_content');
                            }
                            // Set uploadfolder to the image's config's uploadfolder
                            $uploadfolder = isset($GLOBALS['TCA']['tt_content']['columns']['image']['config']['uploadfolder']) ? $GLOBALS['TCA']['tt_content']['columns']['image']['config']['uploadfolder'] : 'uploads/pics';
                            // Set image to relative path and select the first image from the image field
                            $image = $GLOBALS['TSFE']->cObj->listNum($row['image'], 0);
                            if ($image) {
                                $result->image = $GLOBALS['TCA']['tt_content']['columns']['image']['config']['uploadfolder'] . '/' . $image;
                                return true;
                            }
                        }
                    }
                }
            }
        }
        return false;
    }

    private function addTitleTag($searchTag, &$record, &$result)
    {
        if ($searchTag == 'pages') {
            if (empty($result->titleTag)) {
                if (isset($record['tx_seo_titletag']) && !empty($record['tx_seo_titletag'])) {
                    $result->titleTag = $record['tx_seo_titletag'];
                }
            }
        }
    }

    /**
     * @param string $sword
     * @return string
     */
    public function getSuggestResultsAction($sword)
    {

        // make lowercase
        $sword = strtolower(urldecode($sword));

        $suggestTags = array();

        if (!empty($this->settings['suggestResults']['sorting'])) {
            $suggestTags = array_merge($suggestTags, explode(',', trim($this->settings['suggestResults']['sorting'])));
        }

        $suggestTags = array_merge($suggestTags, array_keys($this->settings['suggestResults']['allowedSuggestSearchResult']));

        foreach ($suggestTags as $key => $tag) {
            $suggestTags[$key] = trim($tag);
        }

        $suggestTags = array_unique($suggestTags);


        foreach ($suggestTags as $key => $searchTag) {
            if (intval($this->settings['suggestResults']['allowedSuggestSearchResult'][$searchTag]) == 1) {
                if (isset($searchResults)) {
                    unset($searchResults);
                }

                // add * if it's missing and if the length is less than 8 chars (to prevent not getting any search result)
                if ((substr($sword, -1) != "*") && (strlen($sword) < 5)) {
                    $temp_sword = $sword;
                    $sword .= '*'.' OR '.$sword;
                }

                $this->search = t3lib_div::makeInstance('tx_solr_Search');

                $this->query = t3lib_div::makeInstance('tx_solr_Query', $sword);

                $sword = $temp_sword ? $temp_sword : $sword;
                if (substr($sword, -1) == "*") {
                    $sword = substr($sword, 0, strlen($sword)-1);
                }

                if ($searchTag != 'otherSites') {
                    $this->query->addFilter('type:' . $searchTag);
                }
                $returnFields = $this->settings['specialTags'][$searchTag]['returnFields'];
                if (is_array($returnFields) && (!empty($returnFields))) {
                    $returnFields[] = 'score';
                    $this->query->setFieldList();
                }

                //$this->query->addFilter('language:' . intval (t3lib_div::_GP('L')));

                if ($searchTag != 'otherSites') {
                    // Selects rootline-uid and picks the first/top domain from there and filters the result according to that.
                    $rootPageId = $GLOBALS['TSFE']->rootLine[0]['uid'];
                    $result = $GLOBALS['TYPO3_DB']->exec_selectQuery('domainName', 'sys_domain', 'pid = '.$rootPageId.' AND hidden = 0', '', 'sorting', '0,1');
                    $record = $GLOBALS['TYPO3_DB']->sql_fetch_assoc($result);

                    // get baseURL and strip http*:// and remove trail slash.
                    if (!empty($record['domainName'])) {
                        $cleanUrl = rtrim(preg_replace("/(http:\/\/)/i", "", $record['domainName']), "/");
                        $this->query->addFilter('site:'.$cleanUrl);
                    }
                }

                // Additional filter
                if (!empty($this->settings['additionalFilters'])) {
                    if (isset($this->settings['additionalFilters'][$searchTag])) {
                        foreach ($this->settings['additionalFilters'][$searchTag] as $filter => $filterValue) {
                            $this->query->addFilter($filter.':'.$filterValue);
                        }
                    }
                }
                ////////////////////

                $this->search->search($this->query, $offset, intval($this->settings['suggestResults']['limit']));

                $solrResults = $this->search->getResultDocuments();

                $this->addAdditionalFields($solrResults, $searchTag);

                foreach ($solrResults as $key => $document) {
                    $availableFields = $document->getFieldNames();
                    foreach ($availableFields as $fieldName) {
                        $searchResults[$key][$fieldName] = $document->{$fieldName};
                    }
                    $searchResults[$key]['cHashParams'] = array('cHash' => $searchResults[$key]['contentHash']);
                }
                if ($GLOBALS['TYPO3_CONF_VARS']['SYS']['compat_version'] <= 4.7) {
                    $configuration = Tx_Extbase_Dispatcher::getExtbaseFrameworkConfiguration();
                } else {
                    $configuration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
                }
                if (file_exists(PATH_site . $configuration['view']['partialRootPath'] . ((substr($configuration['view']['partialRootPath'], -1, 1) == '/')?'':'/') .'SuggestResult/'. $searchTag . '.html')) {
                    $this->view->assign('searchTag', $searchTag);
                } else {
                    $this->view->assign('searchTag', 'page');
                }

                $this->view->assign('searchResults', $searchResults);
                $this->view->assign('searchWord', $sword);
                $suggestResult['html'] .= trim($this->view->render());
            }
        }

        return json_encode($suggestResult);
    }

    /**
    * Updates searchstat
    *
    * @param string $searchWord
    * @param string $category
    * @param int $mostRecentAmmount
    * @return void
    */
    private function updateSearchStat($searchword, $category, $mostRecentAmount)
    {

            // Do not try to log empty strings
        if (trim(strlen($searchword)) > 0 &&  trim(strlen($category)) > 0) {
            $searchStat = $this->searchStatRepository->getSearchStatBySearchwordAndCategory($searchword, $category);

            if ($searchStat === null) {

                    // create new SearchStat
                $searchStat = new Pixelant\PxaSolr\Domain\Model\SearchStat();
                    // values only for insert
                $searchStat->setSearchword($searchword);
                $searchStat->setCategory($category);
                $searchStat->setPerformedSearches(1);
            } else {

                    // values only for update
                $searchStat->setPerformedSearches($searchStat->getPerformedSearches() + 1);
            }

                // Same value on update and insert
            $searchStat->setMostRecentAmount($mostRecentAmount);

                // Update or add
            try {
                if ($searchStat->getUid() > 0) {
                    $this->searchStatRepository->update($searchStat);
                } else {
                    $this->searchStatRepository->add($searchStat);
                }
                    // Store to db
                $this->persistenceManager->persistAll();
            } catch (Exception $e) {
            }
        }
    }

    /**
    * Determine if there is a partial template file for current searchtag, else fallbacks to page
    *
    * @param string $searchTag The name of the searchtag used as partial, f.ex. pages, tx_xxx_domain_model_xxx
    * @param string $configuration The extension configuration from ts.
    * @return string The name of the partial that should be used as template.
    */
    private function determinePartial($searchTag, $configuration)
    {
        $partial = 'page';

            // If configuration partialRootPaths exists
        if (is_array($configuration['view']['partialRootPaths'])) {

                // Fetch array of rootpaths if exists
            $partialRootPaths = array_reverse($configuration['view']['partialRootPaths'], true);
            foreach ($partialRootPaths as $key => $value) {

                    // Determine abs path
                $path = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(\TYPO3\CMS\Core\Utility\File\BasicFileUtility::slashPath($value));

                    // If file exists, it is ok to use searchtag as template
                if (file_exists($path . $searchTag . '.html')) {
                    $partial = $searchTag;
                    break;
                }
            }
        } else {
            // If partialRootPath configuration exist
            if (isset($configuration['view']['partialRootPath'])) {

                    // Determine abs path
                $path = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName(\TYPO3\CMS\Core\Utility\File\BasicFileUtility::slashPath($configuration['view']['partialRootPath']));

                    // If file exists, it is ok to use searchtag as template
                if (file_exists($path . $searchTag . '.html')) {
                    $partial = $searchTag;
                }
            }
        }

        return $partial;
    }


    /**
    * Generates a link to frontend search page
    *
    * @return string The form action url
    */
    protected function getFormActionUrl()
    {
        return $this
                ->uriBuilder
                ->setTargetPageUid($this->settings['resultPid'])
                ->setUseCacheHash(false)
                ->setCreateAbsoluteUri(true)
                ->buildFrontendUri();
    }
    
    /**
    * Generates a link to frontend search page
    *
    * @return string The form action url
    */
    protected function getFormJsonActionUrl()
    {
        $linkParams = array(
            'type' => $this->settings['jsonPageType'],
        );

        return $this
                ->uriBuilder
                ->setTargetPageUid($this->settings['resultPid'])
                ->setArguments($linkParams)
                ->setUseCacheHash(false)
                ->setCreateAbsoluteUri(true)
                ->buildFrontendUri();
    }

    /**
    * Generates a link to frontend search page
    *
    * @return string The form action url
    */
    protected function getSuggestUrl()
    {
        $linkParams = array(
            'eID' => 'tx_solr_suggest',
        );

        // Additional filters from typoscript, this is appeneded to suggest_url
        $additionalFilters = $this->getAdditionalFilters();
        if (sizeof($additionalFilters) > 0) {
            $linkParams['filters'] = json_encode($additionalFilters);
        }

        $url = $this
                ->uriBuilder
                ->setArguments($linkParams)
                ->setUseCacheHash(false)
                ->setCreateAbsoluteUri(true)
                ->buildFrontendUri();
            
            // Must include id to get eID to work.
        $url .= '&id=' . $GLOBALS['TSFE']->id;
            // Check if we need to include L param with sys_language_uid
        if ($GLOBALS['TSFE']->sys_language_uid > 0) {
            $url .= '&L=' . $GLOBALS['TSFE']->sys_language_uid;
        }
        return $url;
    }
}
