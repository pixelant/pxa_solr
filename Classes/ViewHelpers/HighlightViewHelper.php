<?php
class Tx_PxaSolr_ViewHelpers_HighlightViewHelper extends Tx_Fluid_Core_ViewHelper_AbstractViewHelper
{

    public function initializeArguments()
    {
        parent::initializeArguments();
        $this->registerArgument('highlight', 'string', 'Highlight string', true);
        $this->registerArgument('subject', 'string', 'Subject string', false);
    }

    /**
     * @return string Rendered string
     */
    public function render()
    {
        $subject = $this->arguments['subject'];
        if (empty($subject)) {
            $subject = $this->renderChildren();
        }

        // Split words by spaces
        $highlights = explode(' ', trim($this->arguments['highlight']));

        // Split words by pipe sign (|)
        foreach ($highlights as $key => $highlight) {
            $pipeSplit = explode('|', $highlight);
            if (!empty($pipeSplit)) {
                unset($highlights[$key]);
                foreach ($pipeSplit as $highlight) {
                    array_push($highlights, $highlight);
                }
            }
        }

        // Split words by plus sign (+)
        foreach ($highlights as $key => $highlight) {
            $plusSplit = explode('+', $highlight);
            if (!empty($plusSplit)) {
                unset($highlights[$key]);
                foreach ($plusSplit as $highlight) {
                    array_push($highlights, $highlight);
                }
            }
        }

        if (empty($highlights)) {
            return $subject;
        }

        $highlights = array_unique($highlights);

        // Convert words into patterns
        foreach ($highlights as $key => $highlight) {
            if (empty($highlight)) {
                unset($highlights[$key]);
            } else {
                $highlights[$key] = '/' . $highlight . '/si';
            }
        }

        // Replace search words with markers to prevent recursive highlighting of highlight spans
        $subject = preg_replace($highlights, '#####\\0%%%%%', $subject);
        
        // Replace markers with highlight spans
        $subject = str_replace('#####', '<span class="highlighted">', $subject);
        $subject = str_replace('%%%%%', '</span>', $subject);
        
        return $subject;
    }
}
