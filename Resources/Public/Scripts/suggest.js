jQuery(document).ready(function(){	

	// this script is dependent on twitter bootstrap typeahead
	jQuery('.search-result-input-sword').typeahead({
		minLength: 1,
		source: function(query, process) {
			jQuery.post(suggest_url, 
				{
					termLowercase: query.toLowerCase(),
					termOriginal: query
				}, function(data) {
				var output = [];
				jQuery.each(data, function(term, termIndex) {
					output.push(term + ' (' + data[term] + ')');
				})
				process(output);
			});
		},
		updater: function (item) {
			if (item.lastIndexOf("(") > 0) {
				item = item.substring(0,item.lastIndexOf("(")-1);	
			}
			jQuery('.search-result-input-sword').parent('form').submit();
			return item;
		},
		sorter: function (items) {
			items.unshift(this.query);
			return items;
		}
	})

});