(function($) {

function MocSearch() {

	var self = this;

	// Slellchecker suggest
    self.spellCheckerWord = null;

	// Action for the search requests
	self.url = null;
	
	// Search hash string
	self.hash = '';

	// Search string
	self.sword = null;

	// Active searchTag
	self.searchTag = '';

	// Active page number
	self.page = {};

	// Number of results to show in the overview list
	self.resultsForOverview = null;

	// List of search tags
	self.searchTags = [];

	// List of active processes
	self.processList = [];

	// Default fadeIn/Out time
	self.fadeTime = 200;

	// Init variables
	self.init = function() {
		// Get the action
		self.url = $('.search-result-form:first').attr('action');

		// Get the number of results to show in the overview
		self.resultsForOverview = parseInt($('.resultsForOverview:first').val(), 10);

		// Get the different search tags
		$.each($('.form-searchtag'), function() {
			self.searchTags.push($(this).val());
		});

		// Atach spellchecker link
        $(document).delegate('a.spellChecker', 'click', self.spellChecker);

		// Attach submit event to form
		$('.search-result-form').submit(self.submit);

		// Attach click event to searchtag-menu items
		$(document).delegate('.searchtag-menu ul li a', 'click', self.switchSearchTag);

		// Attach searchtag view functionality to see more links
		$(document).delegate('.seemore a', 'click', self.switchSearchTag);

		// Attach functionality to page link
		$(document).delegate('.pagelink a', 'click', self.switchPage);

		// Attach functionality to next and previous links
		$(document).delegate('.nextlink a', 'click', self.switchPage);
		$(document).delegate('.prevlink a', 'click', self.switchPage);
	};

	// Method for spellchecker request
    self.spellChecker = function() {
        $('.search-result-form .search-result-input-sword:first').val(self.spellCheckerWord);
        $('.search-result-form:first').submit();
    };

	// Method for aborting current request
	self.abort = function() {
		$.each(self.processList, function(key, process) {
			process.abort();
		});
		self.processList = [];
	};

	// Reset view
	self.resetView = function() {
		// Clean the already existing content and overview
		$('.searchtag-result ul li:not(.pagination)').remove();
		$('.searchoverview-li ul li:not(.seemore)').remove();
		$('.searchtag-result ul li.pagination ul.pages').html('');

		// Hide and clean results and overview
		$('.search-overview').hide();
		$('.search-result').hide();
		$('.search-result ul li.searchtag-result').hide();

		// Reset total count
		$('.searchtag-no-results').html('0');
	};

	// Method for submitting the search form (searching for a new sword)
	self.submit = function(e) {   
		// Get search string
		self.sword = $('.search-result-form .search-result-input-sword:first').val();

		// Perform search if sword is given
		if (self.sword.length > 0) {
    
			// Reset view
			self.resetView();
      
			// Reset active page numbers
			self.page = {};

      self.resuid = $('#searchResultUid').val();
      self.curuid = $('#searchCurrentUid').val();
      
      if (self.resuid == self.curuid) {   // evaluate if this page is search result page
        self.performSearch();
        e.preventDefault();
      } else {

        if ((typeof remember_history !== 'undefined') && (remember_history == '0')) {          // submit form and fetch search-word somewhere else in the code
        } else {
          self.surl = $("form.search-result-form").attr("action");  // get url from action-field in searchform        
          self.surl = self.surl.replace(/[\?&]type(.*)/,"")+'#'+ encodeURIComponent(self.sword);        // remove *type...  stuff from the url and add search-word.

		      window.location = self.surl;    // let's go there.
          
          e.preventDefault();        
        }
        
      }
		} else {
      e.preventDefault();
    }

		
	};

	self.performSearch = function() {
		// Abort all current processes
		self.abort();

		// Remove init from search tag menu
		$('.searchtag-menu.init').removeClass('init');

		// Hide all overviews
		$('.searchoverview-li').hide();

		// Show the overview
		$('.search-overview').show();

		// Send a request for each searchTag
		$.each(self.searchTags, function(key, value) {

			var searchTag = value;
			
			$('#searchtag-menu-item-' + searchTag + ' .searchtag-no-results').html(search_preloader);

			self.processList.push($.ajax({
				url: self.url,
				cache: false,
				data: {

					tx_pxasolr_searchresult: {
						sword: self.sword,
						searchTag: value,
						page: (searchTag === self.searchTag && self.page[value] > 1) ? self.page[value] : 1
					}
				},
				success: function(data, textStatus, XMLHttpRequest) {
					if(data) {
						// Update total count
						var totalCount = parseInt($('#searchtag-menu-item-all .searchtag-no-results').html(), 10);
						totalCount += parseInt(data.totalCount,10);
						$('#searchtag-menu-item-all .searchtag-no-results').html(totalCount);
	
						// Update its own count
						$('#searchtag-menu-item-' + value + ' .searchtag-no-results').html(data.totalCount);
						
						// IDA SPECIFIC!!!: If searchTag == member||discussion && not logged in notify
						if((value === 'member' || value === 'discussion') && parseInt($('#loggedIn').val(), 10) === 0) {
							var content = $('#searchtag-menu-item-' + value + ' .searchtag-no-results').html();
							$('#searchtag-menu-item-' + value + ' .searchtag-no-results').html(content + ' - login required');
						}

						// Update overview and result
						self.updateOverview(value, data);
						self.updateSearchTag(value, data);
		
						// Switch to the active searchTag if a searchTag is set
						if (searchTag === self.searchTag && self.searchTag.length > 0) {
							self.switchSearchTag(self.searchTag);
						}					
					}
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					// Update its own count
					$('#searchtag-menu-item-' + value + ' .searchtag-no-results').html('0');
				}
			}));
		});

		// Update state in url
		self.updateState();
		
		$(document).trigger('MOC_SEARCH_performSearch');
	};

	// Updates a single searchtag in the overview
	self.updateOverview = function(searchTag, data) {
		var count = 0;
		
		if(data.results) {
			$.each(data.results, function(key, value) {
				count++;
	
				if (count <= self.resultsForOverview) {
					$('<li>' + value + '</li>').insertBefore($('#searchoverview-' + searchTag + ' ul li.seemore'));
				}
			});
	
			if(data.results.length > 0) {
				$('#searchoverview-' + searchTag).show();
			}
		}
		
		$(document).trigger('MOC_SEARCH_updateOverview');
	};

	// Update the result page for a searchtag
	self.updateSearchTag = function(searchTag, data) {
		if(data.results && data.results.length > 0) {
			$('#searchtag-result-' + searchTag + ' ul li.pagination').show();
		} else {
			$('#searchtag-result-' + searchTag + ' ul li.pagination').hide();
			if(data.spellChecker) {
				$('<li>' + search_empty + search_spellchecker + '&nbsp;-&nbsp;<a href="javascript:{}" class="spellChecker">' + data.spellChecker + '</a></li>').insertBefore($('#searchtag-result-' + searchTag + ' ul li.pagination'));
				self.spellCheckerWord = data.spellChecker;
			} else {
				$('<li>' + search_empty + '</li>').insertBefore($('#searchtag-result-' + searchTag + ' ul li.pagination'));
			}
		}
		
		// If data contains prev link - insert link
		if (data.prevPage) {
			$('#searchtag-result-' + searchTag + ' ul li.pagination ul.pages').append('<li class="nextlink"><a href="" rel="' + data.prevPage + '">' + search_prev + '</a></li>');
		} else {
			$('#searchtag-result-' + searchTag + ' ul li.pagination ul.pages').append('<li class="nextlink inactive"><span>' + search_prev + '</span></li>');
		}

		if(data.results) {
			$.each(data.results, function(key, value) {
				// Populate the results
				$('<li>' + value + '</li>').insertBefore($('#searchtag-result-' + searchTag + ' ul li.pagination'));
			});
		}

		// If the result contains more than 1 page, insert pagination
		if(data.pagination && data.pagination.length > 1) {
			$.each(data.pagination, function(key, page) {
				if (parseInt(page, 10) > 0) {
					$('#searchtag-result-' + searchTag + ' ul li.pagination ul.pages').append('<li class="pagelink' + ((parseInt(data.currentPage, 10) === parseInt(page, 10)) ? ' active' : '') + '"><a href="" rel="' + page + '">' + page + '</a></li>');
				} else {
					$('#searchtag-result-' + searchTag + ' ul li.pagination ul.pages').append('<li class="ellipsis">' + page + '</li>');
				}
			});
		}

		// If data contains next link - insert link
		if(data.nextPage) {
			$('#searchtag-result-' + searchTag + ' ul li.pagination ul.pages').append('<li class="nextlink"><a href="" rel="' + data.nextPage + '">' + search_next + '</a></li>');
		} else {
			$('#searchtag-result-' + searchTag + ' ul li.pagination ul.pages').append('<li class="nextlink inactive"><span>' + search_next + '</span></li>');
		}

		$('#searchtag-result-' + searchTag + ' ul li.pagination .info .from').html(data.resultsFrom);
		$('#searchtag-result-' + searchTag + ' ul li.pagination .info .to').html(data.resultsTo);
		$('#searchtag-result-' + searchTag + ' ul li.pagination .info .total').html(data.totalCount);

		// Do not show previous and next links if not needed
		if(data.nextPage === null && data.prevPage === null){
			$('#searchtag-result-' + searchTag + ' .nextlink').hide();
		}

		$(document).trigger('MOC_SEARCH_updateSearchTag');
	};

	// Change the view to a searchtag
	self.switchSearchTag = function(searchTag) {

		// IF switchsearchtag is invoked by clicking on, extract searchtag from link class
		if (typeof searchTag !== 'string') {
			searchTag.preventDefault();

			// Find searchTag
			searchTag = $(this).attr('class');
			searchTag = searchTag.split('-');
			searchTag = searchTag[1];
		}
		
		$('.searchtag-menu ul li').removeClass('active');

		if (searchTag === 'overview') {
			$('.search-result').fadeOut(self.fadeTime, function() {
				$('.search-result ul li.searchtag-result').hide();
				$('.search-overview').fadeIn(self.fadeTime);
				$('#searchtag-menu-item-all').addClass('active');
			});

			self.searchTag = ''; // if overview no active search tag or page no
		} else {
			var element;
			if($('.search-overview').css('display') !== 'none') {
				element = $('.search-overview');
			} else {
				element = $('.search-result');
			}

			if((self.searchTag !== searchTag) || ($('.search-result #searchtag-result-' + searchTag).css('display') === 'none')) {
				element.fadeOut(self.fadeTime, function() {
					$('.search-result ul li.searchtag-result').hide();
					$('.search-result').show();
					$('.search-result #searchtag-result-' + searchTag).fadeIn(self.fadeTime);
				});
			}
			
			$('#searchtag-menu-item-' + searchTag).addClass('active');

			// Update active search tag and page
			self.searchTag = searchTag;

			// Page
			var pageNo = parseInt($('.search-result #searchtag-result-' + searchTag).find('.pagination li.active a').html(), 10);
			self.page[searchTag] = pageNo > 0 ? pageNo : 0;
		}

		// Update state in url
		self.updateState();
		
		$(document).trigger('MOC_SEARCH_switchSearchTag');
	};

	// Switch the page inside a search result
	self.switchPage = function(searchTag, page) {
		var elements;
		
		// IF switchsearchtag is invoked by clicking on, extract searchtag from link class
		if (typeof searchTag !== 'string') {
			searchTag.preventDefault();
			
			// Determine searchtag
			searchTag = $(this).parents('.searchtag-result').attr('id');
			searchTag = searchTag.split('-');
			searchTag = searchTag[2];
			
			// Determine requested page
			page = 0;
			if ($(this).parent().hasClass('pagelink')) {
				page = parseInt($(this).attr('rel'), 10);
			} else if ($(this).parent().hasClass('nextlink') || $(this).parent().hasClass('prevlink')) {
				page = parseInt($(this).attr('rel'), 10);
			}
			
			elements = $(this).parents('ul.search-results').find('li:not(.pagination)');
		} else {
			elements = $('#searchtag-result-' + searchTag + ' ul.search-results li:not(.pagination)');
		}

		// Abort all current processes
		self.abort();

		// Update current page
		self.page[searchTag] = page > 0 ? page : 0;

		self.processList.push($.ajax({
			url: self.url,
			cache: false,
			data: {
				tx_pxasolr_searchresult: {
					sword: self.sword,
					searchTag: searchTag,
					page: page
				}
			},
			success: function(data, textStatus, XMLHttpRequest) {
				window.scroll(0,0);

				$('#searchtag-result-' + searchTag).fadeOut(self.fadeTime, function() {
					// Remove the old content
					elements.remove();

					// Insert data
					self.updateSearchTag(searchTag, data);

					$('#searchtag-result-' + searchTag).fadeIn(self.fadeTime);
				});
			}
		}));

		// Update state in url
		self.updateState();
		
		$(document).trigger('MOC_SEARCH_switchPage');
	};


	// Save the state into the URL
	self.updateState = function() {
		var hash = encodeURIComponent(self.sword);

		if (self.searchTag.length > 0) {
			hash += '/' + escape(self.searchTag);
		}

		var key = self.searchTag;
		if (self.page[key] > 1) {
			hash += '/' + self.page[key];
		}
		
		self.hash = hash;

    if ((typeof remember_history !== 'undefined') && (remember_history == '0')) {} else {
		  document.location.hash = hash;
    }
	};

	// Set the state from location.hash
	self.parseState = function(stateString) {

    // if remember_history is off, try to fetch word from variable.
    if ((typeof remember_history !== 'undefined') && (remember_history == '0')) {
      self.sword = postsearch_value;
      return self.validState();
    }

		var stateArr = stateString.match('([a-zA-Z0-9æÆøØåÅäÄöÖ %+\\-|"]+)/?([a-z_]+)?/?([0-9]+)?');
    
		if (stateArr !== null) {
			self.sword = decodeURIComponent(stateArr[1]);
			
			if (typeof stateArr[2] !== 'undefined') {
				self.searchTag = stateArr[2];

				if (typeof stateArr[3] !== 'undefined') {
					var key = self.searchTag;
					self.page[key] = parseInt(stateArr[3], 10);
				}
			}
		}
		return self.validState();
	};

	// Test that the current state is a valid state
	self.validState = function() {

		// Test that sword is string and not empty
		if (typeof self.sword !== 'string' || self.sword.length <= 0) {
			return false;
		}

		// Test that searchTag is valid if given
		// (uses the self.oc() to convert the array into an object literal, so the "in" keyword can be used
		if (typeof self.searchTag !== 'string' || (self.searchTag.length > 0 && !(self.searchTag in self.oc(self.searchTags)))) {
			return false;
		}

		var key = self.searchTag;
		// Test that the page number is valid
		if ((self.page[key] && (typeof self.page[key] !== 'number')) || (parseInt(self.page[key], 10) < 0)) {
			return false;
		}

		// If we got here, the state must be valid
		return true;

	};
	
	self.checkState = function(state) {
		if (typeof search_preloader !== 'undefined')
		// If the passed state parses correctly, perform a search
		if (self.parseState(state)) {
			var selfHash = self.hash.match('([a-zA-Z0-9æÆøØåÅäÄöÖ %+\\-|"]+)/?([a-z_]+)?/?([0-9]+)?');
			var stateHash = state.match('([a-zA-Z0-9æÆøØåÅäÄöÖ %+\\-|"]+)/?([a-z_]+)?/?([0-9]+)?');
			if((selfHash === null) || ((selfHash[1] !== stateHash[1]) && (selfHash[1] !== encodeURIComponent(stateHash[1])))) {
				$('.search-result-form .search-result-input-sword:first').val(self.sword);
				self.resetView();
				self.parseState(state);
				self.performSearch();
			} else if(selfHash[2] !== stateHash[2]) {
				if(stateHash[2]) {
					self.switchSearchTag(stateHash[2]);
				} else if(stateHash[1]) {
					self.switchSearchTag('overview');
				}
			} else if(selfHash[3] !== stateHash[3]) {
				if(stateHash[3]) {
					self.switchPage(stateHash[2], stateHash[3]);
				} else {
					self.switchPage(stateHash[2], 1);
				}
			}
		}
	};

	// Convert an array into an object literal
	self.oc = function(a) {
		var o = {};
		for (var i = 0; i < a.length; i++) {
			o[a[i]] = '';
		}
		return o;
	};
}

$(document).ready(function() {

	// Hide JS required message and show searchresult box
	$('.javascriptrequired').hide();
	$('.tx_mocsearch_pi1').show();

	// Create search-object
	var mocSearch = new MocSearch();
	mocSearch.init();
	
	test = mocSearch;
  
  if ((typeof remember_history !== 'undefined') && (remember_history == '0') && (typeof postsearch_value !== 'undefined') && (postsearch_value.length > 0)) {
   
    mocSearch.checkState(postsearch_value);
    
  } else {
  	$(window).on('hashchange', function(e) {
  		mocSearch.checkState(document.location.hash);
  	})
  	if (document.location.hash != '')
  		mocSearch.checkState(document.location.hash);
  }

});

})(jQuery);
