module.exports = function(grunt) {

  grunt.initConfig({
    dirs: {
      templates: '.',
      output:'../../Public/Scripts'
    },
    watch: {
      handlebars: {
        files: ['<%= handlebars.compile.src %>'],
        tasks: ['handlebars:compile']
      }
    },
    handlebars: {
      compile: {
        src: '<%= dirs.templates %>/*.handlebars',
        dest: '<%= dirs.output %>/handlebars-templates.js'
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-handlebars');
};
